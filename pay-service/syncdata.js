var Sync = require('./models/syncs');
var request = require('request');

module.exports = {
    'startsync': function () {
        var syncFunction = function () {
            var query = Sync.find({});
            query.exec(function (err, syncItems) {

                if (syncItems.length > 0) {
                    console.log('syncing data (' + syncItems.length + ' records)');
                    var i = 0;
                    var successCount = 0;

                    var syncItem = function () {
                        if (i == syncItems.length) {
                            console.log('syncing data success ' + successCount + ' records');
                            setTimeout(syncFunction, 5000);
                        }
                        else
                        {
                            request.post(
                                syncItems[i].url,
                                { json: JSON.parse(syncItems[i].jsonparam), timeout:3000},
                                function (error, response, body) {
                                    if (!error && response.statusCode == 200) {
                                        successCount++;

                                        syncItems[i].remove({}, function (err, resp) {
                                        });
                                    }

                                    i++;
                                    syncItem();
                                }
                            );
                        }
                    }

                    syncItem();
                }
                else {
                    setTimeout(syncFunction, 5000);
                }
            });
        };

        setTimeout(syncFunction, 5000);
    }
};
