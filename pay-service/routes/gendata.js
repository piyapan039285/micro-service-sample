var express = require('express');
var router = express.Router();

router.all('/', function(req, res, next) {
    var Transaction = require('../models/transactions');
    
    Transaction.remove({}, function(err){
        if (err)
            res.json({'success':false, 'message':'', "error":err});
        else
            res.json({'success':true, 'message':''});
        
        // Transaction.insertMany([{
        //     "TxID": "0x10c9e7f9593e705c30d4078317f75cc055a300d8412a974c920ae34de3178371",
        //     "Name": "บ้านฉัน",
        //     "AccID": 1,
        //     "HID": 10,
        //     "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
        //     "Price": 2000,
        //     "Status": 1,
        //     "Type": 1
        // },{
        //     "TxID": "0x4f5105031ddf9775a3787b7790733ea69e518b344bce3d1c8c689c1403b366fc",
        //     "Name": "บ้านทรายทอง",
        //     "AccID": 1,
        //     "HID": 11,
        //     "Date": (new Date('2009-07-16 11:23:00'.split(' ').join('T'))).getTime() / 1000,
        //     "Price": 3000,
        //     "Status": 2,
        //     "Type": 1
        // },
        // {
        //     "TxID": "0x98c4e7f9593e705c30d4078317f75cc055a300d8412a974c920ae34de3178387",
        //     "Name": "บ้านไกล",
        //     "AccID": 2,
        //     "HID": 33,
        //     "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
        //     "Price": 4400,
        //     "Status": 1,
        //     "Type": 1
        // }])
        // .then(function(mongooseDocuments) {
        //     done();
        // })
        // .catch(function(err) {
        //     console.error("dummy data write failed : ", err);
        // });
    });
});

module.exports = router;