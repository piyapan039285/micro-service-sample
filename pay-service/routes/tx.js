var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Transaction = require('../models/transactions');
var Sync = require('../models/syncs');
var request = require('request');

var jwt = require('jsonwebtoken');
var jwtConfig = require('../jwtconfig.js');

function getTransaction(queryParams, res) {
  Transaction.find({ TxID: queryParams.txId }, function (err, dish) {
    if (err)
      throw err;

    res.json(dish);
  });
}

function getTransactionsByUser(queryParams, res) {
  Transaction.find({ AccID: queryParams.id, Status: 1 }, '-_id TxID Name Price Status', function (err, dish) {
    if (err)
      throw err;

    res.json(dish);
  });
}

function uploadSlip(queryParams, res) {
  Transaction.findOne({ TxID: queryParams.id }, '_id Status Image Name Price Date HID', function (err, tx) {
    if (err)
      throw err;
    if (tx != null) {
      tx.set('Image', queryParams.img, { strict: false });
      tx.set('Status', 2, { strict: false });
      tx.save(function (err, updatedTx) {
        if (err)
          res.json({ success: false });

        var jsonparam = { token: queryParams.token, name: tx.get("Name"), hid: tx.get("HID"), price: tx.get("Price"), date: tx.get("Date") };
        request.post(
          'http://hackaton2017.gisc.cdg.co.th/svc/report/add',
          { json: jsonparam, timeout:3000 },
          function (error, response, body) {
            if (!error && response.statusCode == 200) {
              console.log(body)
            }
            else
            {
              console.error("sync to /svc/report/add failed with message", body);
              console.error("putting sync into sync table.");
  
              var newSync          = Sync({
                  // set all of the facebook information in our user model
                  "url": 'http://hackaton2017.gisc.cdg.co.th/svc/report/add',
                  "jsonparam": JSON.stringify(jsonparam)
              }); 
  
              newSync.save(function(err) {
                if (err)
                    throw err;
              });
            }
          }
        );
        res.json({ success: true });
      });
    }
    else
      res.json({ success: true });
  });
}

function addTransactions(queryParams, res) {
  var newTransaction = Transaction({
    "TxID": makeid(),
    "Name": queryParams.name,
    "AccID": queryParams.id,
    "HID": queryParams.hid,
    "Date": (new Date()).getTime() / 1000,
    "Price": queryParams.price,
    "Status": 1,
    "Type": 1
  });
  // save 
  newTransaction.save(function (err) {
    if (err)
      res.json({ success: false });
    res.json({ success: true });
  });
}

function makeid() {
  var text = "0x";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 64; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}


router.use(bodyParser.json());

router.all('/', function (req, res, next) {
  var token = req.cookies["token"] || "";
  jwt.verify(token, jwtConfig.secretKey, function (err, decoded) {
    if (err) {
      return res.json({ success: false, message: 'unauthorized' });
    } else {
      next();
    }
  });
});

router.get('/:txId', function (req, res) {
  getTransaction({ txId: req.params.txId }, res);
});
router.post('/add', function (req, res) {
  jwt.verify((req.body.token || ""), jwtConfig.secretKey, function (err, decoded) {
    if (err) {
      return res.json({ success: false, message: 'unauthorized' });
    } else {
      addTransactions({ id: decoded.id, name: req.body.name, hid: req.body.hid, price: req.body.price }, res);
    }
  });
});
router.post('/lists', function (req, res) {
  jwt.verify((req.cookies["token"] || ""), jwtConfig.secretKey, function (err, decoded) {
    if (err) {
      return res.json({ success: false, message: 'unauthorized' });
    } else {
      getTransactionsByUser({ id: decoded.id }, res);
    }
  });
});
router.post('/upload', function (req, res) {
  var token = req.cookies["token"] || req.body.token || "";
  jwt.verify(token, jwtConfig.secretKey, function (err, decoded) {
    if (err) {
      return res.json({ success: false, message: 'unauthorized' });
    } else {
      uploadSlip({ id: req.body.txID, token: token }, res);
    }
  });
});

module.exports = router;
