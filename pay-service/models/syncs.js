var mongoose = require('mongoose')
var Shema = mongoose.Schema;

//Create schema
var syncSchema = new Shema({
    'url':{
        'type':String,
        'required':true
    },
    'jsonparam':{
        'type':String,
        'required':true
    }
}, { 'timestamps':true});

syncSchema.on('init', function (model) {
  // do stuff with the model after command 'mongoose.model' is completed.
});

//  tell mongodb to create collection 'dishes' and item inside have dishSchema.
//  note that collection 'dish' will be convert to plural -> 'dishes' automatically. 
var syncsModel = mongoose.model('sync', syncSchema);
//  if you have multiple database, 'mongoose.model()' will be changed to 'db.model()' where 'db' variable is 'mongoose.connection' in server1.js file
//var dishesModel = db.model('dish', dishSchema);

//make this module available to node app.
module.exports = syncsModel;