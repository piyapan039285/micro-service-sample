var mongoose = require('mongoose')
var Shema = mongoose.Schema;

//Create schema
var transactionSchema = new Shema({
    'TxID':{
        'type':String,
        'required':true
    },
    'Name':{
        'type':String,
        'required':true
    },
    'Image':{
        'type':String
    },
    'AccID':{
        'type':Number,
        'required':true
    },
    'HID':{
        'type':Number,
        'required':true
    },
    'Date':{
        'type':Number,
        'required':true
    },
    'Price':{
        'type':Number,
        'required':true
    },
    'Status':{
        'type':Number,
        'required':true
    },
    'Type':{
        'type':Number,
        'required':true
    }
}, { 'timestamps':true});

transactionSchema.on('init', function (model) {
  // do stuff with the model after command 'mongoose.model' is completed.
});

//  tell mongodb to create collection 'dishes' and item inside have dishSchema.
//  note that collection 'dish' will be convert to plural -> 'dishes' automatically. 
var transactionsModel = mongoose.model('transaction', transactionSchema);
//  if you have multiple database, 'mongoose.model()' will be changed to 'db.model()' where 'db' variable is 'mongoose.connection' in server1.js file
//var dishesModel = db.model('dish', dishSchema);

//make this module available to node app.
module.exports = transactionsModel;