// load all the things we need
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// load up the user model
var User       = require('../models/users');

// load the auth variables
var configAuth = require('./auth');

module.exports = function(passport) {

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))
    // code for facebook (use('facebook', new FacebookStrategy))
    // code for twitter (use('twitter', new TwitterStrategy))

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,

    },
    function(token, refreshToken, profile, done) {

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {

            // try to find the user based on their google id
            User.findOne({ 'google.id' : profile.id }, function(err, user) {
                if (err)
                    return done(err);

                var email = null;
                var photo = null;

                if(profile.emails != null && profile.emails[0] != null)
                    email = profile.emails[0].value;

                if(profile.photos != null && profile.photos[0] != null)
                    photo = profile.photos[0].value;
                
                if (user) 
                {
                    // if a user is found, just update info

                    user.google.token = token; 
                    user.google.name = profile.displayName;
                    user.google.email = email;
                    user.google.photo = photo;

                    user.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, user);
                    });
                } 
                else 
                {
                    // if the user isnt in our database, create a new user
                    var newUser            = User({
                        // set all of the facebook information in our user model
                        "google" : { 
                            "id": profile.id,
                            "token": token, 
                            "name": profile.displayName,
                            "email": email,
                            "photo": photo
                        }
                    });

                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });

    }));

};