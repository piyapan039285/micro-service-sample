// load all the things we need
var FacebookStrategy = require('passport-facebook').Strategy;

// load up the user model
var User       = require('../models/users');

// load the auth variables
var configAuth = require('./auth');

module.exports = function(passport) {

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        profileFields   : ['id', 'displayName', 'photos', 'email']
    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // find the user in the database based on their facebook id
            User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                var email = null;
                var photo = null;

                if(profile.emails != null && profile.emails[0] != null)
                    email = profile.emails[0].value;

                if(profile.photos != null && profile.photos[0] != null)
                    photo = profile.photos[0].value;

                // if the user is found, then log them in
                if (user) 
                {
                    user.facebook.token = token; 
                    user.facebook.name = profile.displayName;
                    user.facebook.email = email;
                    user.facebook.photo = photo;

                    user.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, user);
                    });
                } else {

                    // if there is no user found with that facebook id, create them
                    var newUser            = User({
                        // set all of the facebook information in our user model
                        "facebook" : { 
                            "id": profile.id, // set the users facebook id                   
                            "token": token, // we will save the token that facebook provides to the user                    
                            "name": profile.displayName, // look at the passport user profile to see how names are returned
                            "email": email, // facebook can return multiple emails so we'll take the first
                            "photo": photo
                        }
                    });

                    // save our user to the database
                    newUser.save(function(err) {
                        if (err)
                            throw err;

                        // if successful, return the new user
                        return done(null, newUser);
                    });
                }

            });
        });

    }));

};