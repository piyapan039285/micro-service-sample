var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var mongoose = require('mongoose');
var url = 'mongodb://localhost:4000/test';
mongoose.connect(url, {useMongoClient: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to mongodb");

    var passport = require('passport');

    var myFacebookPassport = require('./config/facebookPassport');
    myFacebookPassport(passport);
    
    var myGooglePassport = require('./config/googlePassport');
    myGooglePassport(passport);

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(require('express-session')({ secret: 'P@ssw0rd', resave: true, saveUninitialized: true }));
    // app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.static('public'))
    app.use(passport.initialize());
    app.use(passport.session());

    var mustLoggedIn = function(req, res, next) {
        if (req.user) {
            next();
        } else {
            res.redirect('/facebooklogin');
        }
    }

    app.use('/svc/login', require('./routes/index'));
    app.use('/svc/login/failed', require('./routes/loginfailed'));
    app.use('/svc/login/profile', mustLoggedIn, require('./routes/profile'));
       
    app.get('/svc/login/facebooklogin', passport.authenticate('facebook', {'scope':["public_profile", "email", "user_friends"]}));
    
    app.get('/svc/login/facebooklogin/callback',
      passport.authenticate('facebook', { failureRedirect: '/svc/login/failed' }),
      
      function(req, res) {
        // Successful authentication, redirect home. 
        
        var jwt = require('jsonwebtoken');
        var jwtConfig = require('./jwtconfig.js');
        var token = jwt.sign({
            "id": 1, //req.user.facebook.id, 
            "email": req.user.facebook.email, 
            "name": req.user.facebook.name, 
            "photo":req.user.facebook.photo
        }, jwtConfig.secretKey, {
            expiresIn : 60*60*24 // expires in 24 hours
        });
        res.cookie('token', token);
      
        //res.redirect('/svc/login/profile');
        res.redirect('/search');
    });    

    app.get('/svc/login/googlelogin', passport.authenticate('google', {
        'scope':[
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile'
    ]}));

    app.get('/svc/login/googlelogin/callback', 
        passport.authenticate('google', { failureRedirect: '/svc/login/failed' }),
        function(req, res) {
        // Successful authentication, redirect home.

        var jwt = require('jsonwebtoken');
        var jwtConfig = require('./jwtconfig.js');
        var token = jwt.sign({
            "id": 1, //req.user.google.id, 
            "email": req.user.google.email, 
            "name": req.user.google.name,
             "photo":req.user.google.photo
            }, jwtConfig.secretKey, {
            expiresIn : 60*60*24 // expires in 24 hours
        });
        res.cookie('token', token);
        //res.json({});
        res.redirect('/search');
        //res.redirect('/svc/login/profile');
    });

     // catch 404 and forward to error handler
     app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });
    
    // error handler
    app.use(function(err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};
    
      // render the error page
      res.status(err.status || 500);
      res.render('error');
    });
});

module.exports = app;
