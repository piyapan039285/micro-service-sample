var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  if(req.user.facebook != null)
    res.render('profile_facebook', { 'title': 'Profile', 'user': req.user });
  else
    res.render('profile_google', { 'title': 'Profile', 'user': req.user });
});

module.exports = router;
