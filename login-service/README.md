# Setup
1. open command prompt and run ```npm install```
2. create folder \mongodb\data
3. Open command prompt and cd to \mongodb and run ```mongod --dbpath=data --port 4000```
4. run website by typing ```npm start```
5. To login facebook, go to url ```http://hackaton2017.gisc.cdg.co.th/svc/login/facebooklogin```

# Remove Permission from Account
## Facebook
Go to 'Account Settings' -> 'Apps' (```https://www.facebook.com/settings?tab=applications```) and remove app 'Hackaton2017'
## Google
Go to ```https://myaccount.google.com/permissions``` and remove app 'Hackaton2017'