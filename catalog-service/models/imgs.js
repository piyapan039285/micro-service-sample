var mongoose = require('mongoose')
var Shema = mongoose.Schema;

//Create schema
var imgSchema = new Shema({
    'HID':{
        'type':Number,
        'required':true
    },
    'ImageUrl':{
        'type':String,
        'required':true
    },
    'Date':{
        'type':Number,
        'required':true
    }
}, { 'timestamps':true});

imgSchema.on('init', function (model) {
  // do stuff with the model after command 'mongoose.model' is completed.
});

//  tell mongodb to create collection 'dishes' and item inside have dishSchema.
//  note that collection 'dish' will be convert to plural -> 'dishes' automatically. 
var imgsModel = mongoose.model('img', imgSchema);
//  if you have multiple database, 'mongoose.model()' will be changed to 'db.model()' where 'db' variable is 'mongoose.connection' in server1.js file
//var dishesModel = db.model('dish', dishSchema);

//make this module available to node app.
module.exports = imgsModel;