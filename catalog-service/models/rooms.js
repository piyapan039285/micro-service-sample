var mongoose = require('mongoose')
var Shema = mongoose.Schema;

//Create schema
var roomSchema = new Shema({
    'AccID': {
        'type': Number,
        'required': true
    },
    'HID': {
        'type': Number,
        'required': true
    },
    'Date': {
        'type': Number,
        'required': true
    },
    'Name': {
        'type': String,
        'required': true
    },
    'Addr': {
        'type': String
    },
    'Size': {
        'type': String
    },
    'Price': {
        'type': Number,
        'required': true
    },
    'Deposit': {
        'type': Number
    },
    'Tel': {
        'type': String
    },
    'Available': {
        'type': Number,
        'required': true
    },
    'location': { 'type': { type: String, enum: "Point", default: "Point" }, coordinates: { type: [Number], default: [0, 0] } }
}, { 'timestamps': true });

roomSchema.on('init', function (model) {
    // do stuff with the model after command 'mongoose.model' is completed.
});

//  tell mongodb to create collection 'dishes' and item inside have dishSchema.
//  note that collection 'dish' will be convert to plural -> 'dishes' automatically. 
roomSchema.index({ 'location': '2dsphere' });
var roomsModel = mongoose.model('room', roomSchema);
//  if you have multiple database, 'mongoose.model()' will be changed to 'db.model()' where 'db' variable is 'mongoose.connection' in server1.js file
//var dishesModel = db.model('dish', dishSchema);

//make this module available to node app.
module.exports = roomsModel;