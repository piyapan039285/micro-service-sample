#!/bin/bash

test -d /home/administrator/microservice/catalog-service/mongodb && sudo rm -r /home/administrator/microservice/catalog-service/mongodb
mkdir /home/administrator/microservice/catalog-service/mongodb
mkdir /home/administrator/microservice/catalog-service/mongodb/data 

hostIP=$(ifconfig enp0s3 | awk '/inet addr/{print substr($2,6)}')

docker run --detach \
    --publish 3300:3300 \
    --name catalog-service \
    --add-host hackaton2017.gisc.cdg.co.th:$hostIP \
    --volume /home/administrator/microservice/catalog-service:/app \
    catalog-service-image
