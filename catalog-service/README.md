# Setup
1. open command prompt and run ```npm install```
2. create folder \mongodb\data
3. Open command prompt and cd to \mongodb and run ```mongod --dbpath=data --port 4400```
4. run website by typing ```npm start```
5. go to url ```http://hackaton2017.gisc.cdg.co.th/svc/catalog```, main page is ```http://hackaton2017.gisc.cdg.co.th/search```

# Clear & Insert Dummy Data
go to url ```http://hackaton2017.gisc.cdg.co.th/svc/catalog/gendata```