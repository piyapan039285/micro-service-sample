var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Room = require('../models/rooms');
var IMG = require('../models/imgs');

function getRoomDetail(queryParams, res) {
  Room.findOne({ HID: queryParams.HID }, '-_id Name Addr Tel Size Price Deposit', function (err, room) {
    if (err)
      throw err;
    IMG.find({ HID: queryParams.HID }, '-_id ImageUrl', function (err, imgs) {
      if (err || imgs.length == 0)
        res.json(room);
      else {
        room.set('images', imgs, { strict: false });
        res.json(room);
      }
    });
  });
}

router.use(bodyParser.json());

router.all('/', function (req, res, next) {
  next(); //tell that the request is not end.
});

router.post('/', function (req, res) {
  // res.setHeader(200, { 'Content-Type': 'application/json' });
  getRoomDetail({ HID: req.body.HID }, res);
});

module.exports = router;
