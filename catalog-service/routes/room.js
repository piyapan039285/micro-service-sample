var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Room = require('../models/rooms');
var IMG = require('../models/imgs');

function findRoomByRange(queryParams, res) {
  var min = queryParams.min, max = queryParams.max;
  var where = { '$gte': queryParams.min, '$lte': queryParams.max };
  if (min && !max)
    where = { '$gte': queryParams.min }
  else if (max && !min)
    where = { '$lte': queryParams.max };
  var query = Room
    .find({ Price: where, Available: 1 })
    .sort('Price')
    .select('-_id HID Name Addr Tel Price');
  query.exec(function (err, room) {
    if (err)
      throw err;
    res.json(room);
  })
}

function findRoomByLocation(queryParams, res) {
  Room.find(
    {
      "location": {
        "$nearSphere": {
          "$geometry": {
            "type": "Point", "coordinates": queryParams.geo
          },
          "$maxDistance": queryParams.radius, /* 124.274238 miles */
        }
      },
      "Available": 1 
    }
    ,"-_id HID Name Addr Tel Size Deposit Price location", function (err, room) {
      if (err)
        throw err;
      res.json(room);
    });
  // Room.find({ geo: { $nearSphere: queryParams.geo, $maxDistance: queryParams.radius } }, function (err, room) {
  //   if (err)
  //     throw err;
  //   res.json(room);
  // });
}

router.use(bodyParser.json());

router.all('/', function (req, res, next) {
  next(); //tell that the request is not end.
});
router.post('/', function (req, res) {
  // res.setHeader(200, { 'Content-Type': 'application/json' });
  findRoomByRange({ min: req.body.min, max: req.body.max }, res);
});

router.post('/locations', function (req, res) {
  // res.setHeader(200, { 'Content-Type': 'application/json' });
  findRoomByLocation({ geo: [parseFloat(req.body.lon), parseFloat(req.body.lat)], radius: req.body.radius }, res);
});

router.post('/all', function (req, res) {
  // res.setHeader(200, { 'Content-Type': 'application/json' });
  Room.find({}, function (err, dish) {
    if (err)
      throw err;

    res.json(dish);
  });
});

module.exports = router;
