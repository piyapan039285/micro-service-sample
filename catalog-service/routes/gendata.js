var express = require('express');
var router = express.Router();

router.all('/', function(req, res, next) 
{
    var Room = require('../models/rooms');
    var Img = require('../models/imgs');
    Room.remove({}, function () {
        var _dummyRoom = [
            {
                "AccID": 1,
                "HID": 1,
                "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
                "Name": "บ้านป้าแย้ม",
                "Addr": "54/173 ถนน นนทรี Khwaeng Chong Nonsi, Khet Yan Nawa, Krung Thep Maha Nakhon 10120",
                "Tel": "02-294-7368",
                "Size": "30x30",
                "Deposit": 1,
                "Price": 2000,
                "Available": 1,
                "location": {
                    "type": "Point",
                    "coordinates": [
                        100.53814829999828,
                        13.695162399999768
                    ]
                }
            },
            {
                "AccID": 1,
                "HID": 2,
                "Date": (new Date('2009-07-16 11:23:00'.split(' ').join('T'))).getTime() / 1000,
                "Name": "บ้านป้ายิ้ม",
                "Addr": "14/105 ถนน นนทรี Khwaeng Chong Nonsi, Khet Yan Nawa, Krung Thep Maha Nakhon 10120",
                "Tel": "08-296-7777",
                "Size": "40x40",
                "Deposit": 2,
                "Price": 7500,
                "Available": 1,
                "location": {
                    "type": "Point",
                    "coordinates": [
                        100.55614684951007,
                        13.784406002440045
                    ]
                }
            },
            {
                "AccID": 1,
                "HID": 3,
                "Date": (new Date('2010-07-18 11:23:00'.split(' ').join('T'))).getTime() / 1000,
                "Name": "บ้านป้าย้อย",
                "Addr": "77/33 ถนน นนทรี Khwaeng Chong Nonsi, Khet Yan Nawa, Krung Thep Maha Nakhon 10120",
                "Tel": "08-999-9999",
                "Size": "24x24",
                "Deposit": 2,
                "Price": 500,
                "Available": 1,
                "location": {
                    "type": "Point",
                    "coordinates": [
                        100.61931823622872,
                        13.705702162319064
                    ]
                }
            },
            {
                "AccID": 1,
                "HID": 4,
                "Date": (new Date('2012-05-18 11:23:00'.split(' ').join('T'))).getTime() / 1000,
                "Name": "บ้านถํ้าเสือ",
                "Addr": "555 ถนน นนทรี Khwaeng Chong Nonsi, Khet Yan Nawa, Krung Thep Maha Nakhon 10120",
                "Tel": "08-000-000",
                "Size": "45x45",
                "Deposit": 2,
                "Price": 4500,
                "Available": 1,
                "location": {
                    "type": "Point",
                    "coordinates": [
                        100.563699950096,
                        13.669009148897034
                    ]
                }
            },
            {
                "AccID": 1,
                "HID": 5,
                "Date": (new Date('2017-03-18 11:23:00'.split(' ').join('T'))).getTime() / 1000,
                "Name": "บ้านทรายทอง",
                "Addr": "777 ถนน นนทรี Khwaeng Chong Nonsi, Khet Yan Nawa, Krung Thep Maha Nakhon 10120",
                "Tel": "09-554-000",
                "Size": "100x100",
                "Deposit": 2,
                "Price": 15000,
                "Available": 1,
                "location": {
                    "type": "Point",
                    "coordinates": [
                        100.47168945204925,
                        13.79374198939862
                    ]
                }
            },
            {
                "AccID": 1,
                "HID": 6,
                "Date": (new Date('2017-07-18 11:23:00'.split(' ').join('T'))).getTime() / 1000,
                "Name": "บ้านคุณปุ้ม",
                "Addr": "123 ถนน นนทรี Khwaeng Chong Nonsi, Khet Yan Nawa, Krung Thep Maha Nakhon 10120",
                "Tel": "08-88888-000",
                "Size": "35x35",
                "Deposit": 2,
                "Price": 2500,
                "Available": 1,
                "location": {
                    "type": "Point",
                    "coordinates": [
                        100.50876830947107,
                        13.612957884008171
                    ]
                }
            }
        ];
        Room.insertMany(_dummyRoom)
            .then(function (mongooseDocuments) {
                Img.remove({}, function(err) {
                    if (err)
                    {
                        res.json({'success':false, 'message':'', "error":err});
                        return;
                    }
                    
                    var _dummyImgs = [];
                    for (var i = 0; i < _dummyRoom.length; i++) {
                        _dummyImgs.push(
                            {
                                "HID": _dummyRoom[i].HID,
                                "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
                                "ImageUrl": "http://www.xn--12cfra9ebbh1dya8moa2e0n.com/ckfinder/userfiles/images/bedroom/%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%99%E0%B8%AD%E0%B8%99%E0%B8%AA%E0%B8%B5%E0%B8%84%E0%B8%A3%E0%B8%B5%E0%B8%A12%281%29.jpg"
                            },
                            {
                                "HID": _dummyRoom[i].HID,
                                "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
                                "ImageUrl": "http://fb1-cb.lnwfile.com/w0b7zo.jpg"
                            },
                            {
                                "HID": _dummyRoom[i].HID,
                                "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
                                "ImageUrl": "http://j.lnwfile.com/_/j/_raw/rn/wg/d4.jpg"
                            },
                            {
                                "HID": _dummyRoom[i].HID,
                                "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
                                "ImageUrl": "http://www.xn--12cfra9ebbh1dya8moa2e0n.com/ckfinder/userfiles/images/bathroom/calm_bathroom/%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%AA%E0%B8%B5%E0%B8%84%E0%B8%A3%E0%B8%B5%E0%B8%A112.jpg"
                            },
                            {
                                "HID": _dummyRoom[i].HID,
                                "Date": (new Date('2009-07-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
                                "ImageUrl": "http://4.bp.blogspot.com/-bELYiS8qkQs/UXKbEfNGZpI/AAAAAAAABHI/J_Z81hHGzws/s1600/%25E0%25B8%25AB%25E0%25B9%2589%25E0%25B8%25AD%25E0%25B8%2587%25E0%25B8%2584%25E0%25B8%25A3%25E0%25B8%25B1%25E0%25B8%25A7%25E0%25B8%2595%25E0%25B9%2588%25E0%25B8%25AD%25E0%25B9%2580%25E0%25B8%2595%25E0%25B8%25B4%25E0%25B8%25A13.jpg"
                            });
                    }
                    Img.insertMany(_dummyImgs)
                        .then(function (mongooseDocuments) {
                            res.json({'success':true, 'message':''});
                        })
                        .catch(function (err) {
                            res.json({'success':false, 'message':"img dummy data write failed", "error":err});
                        });
                });
            })
            .catch(function (err) {
                res.json({'success':false, 'message':"room dummy data write failedd", "error":err});
            });
    });
});

module.exports = router;