var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Room = require('../models/rooms');
var Sync = require('../models/syncs');
var request = require('request');

var jwt = require('jsonwebtoken');
var jwtConfig = require('../jwtconfig.js');

function bookingHouse(queryParams, res) {
  Room.findOne({ HID: queryParams.HID }, '_id Available Name HID Price', function (err, room) {
    if (err)
      throw err;
    room.set('Available', 0, { strict: false });
    room.save(function (err, updatedRoom) {
      if (err)
        res.json({ success: false });

      var jsonparam = { token: queryParams.token, name: room.get("Name"), hid: queryParams.HID, price: room.get("Price") };

      request.post(
        'http://hackaton2017.gisc.cdg.co.th/svc/pay/tx/add',
        { json: jsonparam, timeout:3000},
        function (error, response, body) {
          if (!error && response.statusCode == 200) {
            
          }
          else
          {
            console.error("sync to /svc/pay/tx/add failed with message", body);
            console.error("putting sync into sync table.");

            var newSync          = Sync({
                // set all of the facebook information in our user model
                "url": 'http://hackaton2017.gisc.cdg.co.th/svc/pay/tx/add',
                "jsonparam": JSON.stringify(jsonparam)
            }); 

            newSync.save(function(err) {
              if (err)
                  throw err;
            });
          }
        }
      );
      res.json({ success: true });
    });
  });
}

router.use(bodyParser.json());

router.all('/', function (req, res, next) {
  next(); //tell that the request is not end.
});

router.post('/', function (req, res) {
  // res.setHeader(200, { 'Content-Type': 'application/json' });
  var token = req.cookies["token"] || req.body.token || "";
  jwt.verify(token, jwtConfig.secretKey, function (err, decoded) {
    if (err) {
      return res.json({ success: false, message: 'unauthorized' });
    } else {
      bookingHouse({ token: token, HID: req.body.HID }, res);
    }
  });
});

module.exports = router;
