#!/bin/bash

test -d /home/administrator/microservice/report-service/mongodb && sudo rm -r /home/administrator/microservice/report-service/mongodb
mkdir /home/administrator/microservice/report-service/mongodb
mkdir /home/administrator/microservice/report-service/mongodb/data 

hostIP=$(ifconfig enp0s3 | awk '/inet addr/{print substr($2,6)}')

docker run --detach \
    --publish 3100:3100 \
    --name report-service \
    --add-host hackaton2017.gisc.cdg.co.th:$hostIP \
    --volume /home/administrator/microservice/report-service:/app \
    report-service-image
