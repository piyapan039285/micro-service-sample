# Setup
1. open command prompt and run ```npm install```
2. create folder \mongodb\data
3. Open command prompt and cd to \mongodb and run ```mongod --dbpath=data --port 4200```
4. run website by typing ```npm start```
5. go to url ```http://hackaton2017.gisc.cdg.co.th/svc/report/genreport```

# Clear & Insert Dummy Data
go to url ```http://hackaton2017.gisc.cdg.co.th/svc/report/gendata```