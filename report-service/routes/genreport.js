var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Report = require('../models/reports');

var jwt = require('jsonwebtoken');
var jwtConfig = require('../jwtconfig.js');

function genreport(queryParams, res) {
  var startDate = new Date(queryParams.year + '-01-01T00:00:01').getTime() / 1000;
  var endDate = new Date(queryParams.year + '-12-31T00:00:01').getTime() / 1000;
  var min = queryParams.min, max = queryParams.max;
  var where = { '$gte': startDate, '$lte': endDate };
  var query = Report
    .find({ Date: where })
    .sort('Date')
    .select('-_id Date Name Price');
  query.exec(function (err, reports) {
    if (err)
      throw err;
    var data = initReportData();
    for (var index in reports) {
      var report = reports[index];
      var month = getMonthFromTimeStamp(report._doc.Date);
      data[month].total += report._doc.Price;
      data[month].data.push([report._doc.Name, report._doc.Price]);
    }
    res.json(data);
  })
}

function getMonthFromTimeStamp(time) {
  return (new Date(time * 1000)).getMonth();
}

function initReportData() {
  var _data = [];
  for (var i = 0; i < 12; i++) {
    _data.push({
      month: i + 1,
      total: 0,
      data: []
    });
  }
  return _data;
}

router.use(bodyParser.json());
router.route('/')
  .all(function (req, res, next) {
    //res.setHeader(200, { 'Content-Type': 'application/json' });
    var token = req.cookies["token"] || req.body.token || "";

    jwt.verify(token, jwtConfig.secretKey, function (err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'unauthorized' });
      } else {
        next();
      }
    });
  })
  .get(function (req, res, next) {
    genreport({}, res);
  })
  .post(function (req, res, next) {
    genreport({ year: req.body.year }, res);
  })
router.post('/all', function (req, res) {
  Report.find({}, function (err, dish) {
    if (err)
      throw err;

    res.json(dish);
  });
});


module.exports = router;
