var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Report = require('../models/reports');

var jwt = require('jsonwebtoken');
var jwtConfig = require('../jwtconfig.js');

function addReport(queryParams, res) {
  var newReport = Report({
    "Name": queryParams.name,
    "AccID": queryParams.id,
    "HID": queryParams.hid,
    "Date": queryParams.date,
    "Price": queryParams.price
  });
  // save 
  newReport.save(function (err) {
    if (err)
      res.json({ success: false });
    res.json({ success: true });
  });
}

router.use(bodyParser.json());
router.post('/', function (req, res) {
  var token = req.cookies["token"] || req.body.token || "";
  jwt.verify(token, jwtConfig.secretKey, function (err, decoded) {
    if (err) {
      return res.json({ success: false, message: 'unauthorized' });
    } else {
      addReport({ id: decoded.id, name: req.body.name, hid: req.body.hid, price : req.body.price, date : req.body.date }, res);
    }
  });
});

module.exports = router;
