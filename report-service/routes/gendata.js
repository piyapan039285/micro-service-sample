var express = require('express');
var router = express.Router();

router.all('/', function(req, res, next) {
    var Report = require('../models/reports');
    
    Report.remove({}, function(err){
        if (err)
            res.json({'success':false, 'message':'', "error":err});
        else
            res.json({'success':true, 'message':''});
            
        // Report.insertMany([{
        //     "AccID": 1,
        //     "HID": 100,
        //     "Name": "บ้านสมชาย",
        //     "Date": (new Date('2017-02-15 08:01:00'.split(' ').join('T'))).getTime() / 1000,
        //     "Price": 2000
        // },{
        //     "AccID": 1,
        //     "HID": 200,
        //     "Name": "บ้านสมศรี",
        //     "Date": (new Date('2017-01-16 11:23:00'.split(' ').join('T'))).getTime() / 1000,
        //     "Price": 3000
        // },
        // {
        //     "AccID": 1,
        //     "HID": 300,
        //     "Name": "บ้านสมบูรณ์",
        //     "Date": (new Date('2017-02-16 08:01:00'.split(' ').join('T'))).getTime() / 1000,
        //     "Price": 500
        // }])
        // .then(function(mongooseDocuments) {
        //     res.json({'success':true, 'message':''});
        // })
        // .catch(function(err) {
        //     res.json({'success':false, 'message':"dummy data write failed", "error":err});
        // });
    });
});

module.exports = router;