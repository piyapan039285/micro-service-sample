var mongoose = require('mongoose')
var Shema = mongoose.Schema;

//Create schema
var reportSchema = new Shema({
    'AccID':{
        'type':String,
        'required':true
    },
    'Name':{
        'type':String,
        'required':true
    },
    'HID':{
        'type':Number,
        'required':true
    },
    'Date':{
        'type':Number,
        'required':true
    },
    'Price':{
        'type':Number
    }
}, { 'timestamps':true});

reportSchema.on('init', function (usersModel) {
  // do stuff with the model after command 'mongoose.model' is completed.
});

//  tell mongodb to create collection 'dishes' and item inside have dishSchema.
//  note that collection 'dish' will be convert to plural -> 'dishes' automatically. 
var reportsModel = mongoose.model('report', reportSchema);
//  if you have multiple database, 'mongoose.model()' will be changed to 'db.model()' where 'db' variable is 'mongoose.connection' in server1.js file
//var dishesModel = db.model('dish', dishSchema);

//make this module available to node app.
module.exports = reportsModel;
