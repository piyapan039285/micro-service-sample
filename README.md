This is simple hotel-renting website using micro-service concept. I divide this project into 4 micro-services as follows

1. login service
1. catalog service : used for storing and listing hotel rooms
1. report service : used for generating reports
1. payment service : used for making payments (just fake payment)

# Requirements
1. NodeJS 6 : https://nodejs.org/en/
2. MongoDB Community : https://www.mongodb.com/download-center#community
3. Add ```C:\Program Files\MongoDB\Server\3.4\bin``` to PATH
4. Add ```127.0.0.1 hackaton2017.gisc.cdg.co.th``` to hosts file

# Setup
1. Disable IIS Service
2. extract \nginx\nginx.zip into folder \nginx
3. open command prompt as admin, cd to folder \nginx
4. type ```start nginx```
5. follow README.md in each subfolder.

# Stop NGINX
type ```nginx -s stop```,  ```nginx -s quit```

# Reload NGINX
type ```nginx -s reload```


# Deploy
## Software Requirements
* Ubuntu 16.04 LTS
* Docker : https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-using-the-repository
* Nginx Community : https://www.nginx.com/resources/admin-guide/installing-nginx-open-source/
* Git : https://help.ubuntu.com/lts/serverguide/git.html

## Step
0. Open Ubuntu.
1. Create folder ```/home/administrator/microservice```
2. Clone project from git into this folder. (You **CANNOT COPY** project folder from windows into ubuntu because new line encoding is different between Windows and Linux)
3. Type command ```sudo nano /etc/nginx/sites-available/default```
4. You have to copy code in file ```<git repo>/nginx/conf/nginx.conf``` only section ```/svc/...``` (Ex. "location /svc/login") into ```/etc/nginx/sites-available/default```.
5. Type command ```nginx -s reload``` to reload script. 
6. Run ```<git repo>/my_docker_base/build.sh```
7. Run ```<git repo>/deploy.sh```

Note : If you want to view docker message, don't run deploy.sh. You have to run file 'run.sh' manually in each folder without '--detach' option.